import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Container, Row, Col, InputGroup, Input, InputGroupAddon, 
  Button, ButtonGroup, Form, FormGroup, Alert, ListGroup, ListGroupItem,
  Card, CardTitle, CardText } from "reactstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faQuestion, faStarOfDavid, faStarHalf, faSmileWink } from '@fortawesome/free-solid-svg-icons'

class App extends Component {
  constructor(props){
    super(props)

    this.input1Ref = React.createRef();
    this.input2Ref = React.createRef();

    this.state = {
      checks: [
        {
          type: "CHECK",
          id: "emptyFields",
          text: "С пустыми полями все хорошо",
          isDone: false
        },
        {
          type: "CHECK",
          id: "zeroAndZero",
          text: "0 и 0 - правильный ход",
          isDone: false
        },
        {
          type: "CHECK",
          id: "zeroAndNormal",
          text: "Взаимодействие с 0 тоже не лишнее",
          isDone: false
        },
        {
          type: "CHECK",
          id: "middleValuesAboveZero",
          text: "Ну как же без значений из класса эквивалентности",
          isDone: false
        },
        {
          type: "CHECK",
          id: "middleValuesBelowZero",
          text: "Ну да, про второй класс тоже не нужно забывать",
          isDone: false
        },
        {
          type: "CHECK",
          id: "upper",
          text: "А вот и верхний предел",
          isDone: false
        },
        {
          type: "CHECK",
          id: "lower",
          text: "Без нижнего предела тоже никуда",
          isDone: false
        },
        {
          type: "CHECK",
          id: "deductBelowZero",
          text: "С отрицательными числами работать научили, спасибо",
          isDone: false
        },
        {
          type: "CHECK",
          id: "belowLower",
          text: "С нижней границей все в порядке",
          isDone: false
        },
        {
          type: "CHECK",
          id: "aboveUpper",
          text: "Да и с верхней границей разобрались",
          isDone: false
        },
        {
          type: "CHECK",
          id: "floatDot",
          text: "Нет, с дробными числами мы не работаем",
          isDone: false
        },
        {
          type: "CHECK",
          id: "specialChars",
          text: "Ишь чего удумал, не буду я такое считать",
          isDone: false
        },
        {
          type: "CHECK",
          id: "nonNumeric",
          text: "Буквы в слова я складывать тоже не умею",
          isDone: false
        },
        {
          type: "CHECK",
          id: "divideByZero",
          text: "На ноль делить нельзя, по крайней мере в моей системе исчисления",
          isDone: false
        },
        {
          type: "CHECK",
          id: "plus",
          text: "Сложение проверили",
          isDone: false
        },
        {
          type: "CHECK",
          id: "deduct",
          text: "Про вычитание не забыли",
          isDone: false
        },
        {
          type: "CHECK",
          id: "multiply",
          text: "Умножение тоже важно",
          isDone: false
        },
        {
          type: "CHECK",
          id: "divide",
          text: "Ну и самое хитрое — деление",
          isDone: false
        },
        {
          type: "EGG",
          id: "EGG",
          text: "Иногда и задание хранит в себе секрет",
          isDone: false
        }
      ],
      outputValue: "",
      showAlert: false,
      alertText: "",
      successAlert: false,
      allDone: false,
      easterEggBeginButtonOpacity: 0,
      easterEggShowQuestion: false,
      easterEggShowMem: false
    }
  }

  toggleSuccessAlert(){
    this.setState({
      successAlert: true
    });
    setTimeout(() => {
      this.setState({
        successAlert: false
      });
    }, 3500)
  }

  toggleErrorAlert(text){
    this.setState({
      showAlert: true,
      alertText: text,
      outputValue: ""
    });
    setTimeout(() => {
      this.setState({
        showAlert: false
      });
    }, 3500)
  }

  allDone(){
    let currentChecks = this.state.checks;
    let done = true;
    currentChecks.forEach((obj) => {
      if (!obj.isDone){
        done = obj.isDone;
      }
    });
    this.setState({
      allDone: done
    });
  }

  setCheckToDone(checkId, ignoreAlert){
    let currentChecks = this.state.checks;
    if (!currentChecks.find((x) => x.id === checkId).isDone){
      currentChecks.forEach((obj) => {
        if (obj.id === checkId){
          obj.isDone = true
        }
      })
      this.setState({
        checks: currentChecks
      })
      if (!ignoreAlert){
        this.toggleSuccessAlert()
      }
    }
    this.allDone()
  }

  verifyEasterEgg(e) {
    const valueOne = ReactDOM.findDOMNode(this.input1Ref.current).value;
    const valueTwo = ReactDOM.findDOMNode(this.input2Ref.current).value;
    let opacity = 0;
    const arrayOne = ["Ч", "Чи", "Чис", "Числ", "Число", "Число ", "Число 1"];
    const arrayTwo = ["Ч", "Чи", "Чис", "Числ", "Число", "Число ", "Число 2"];
    for (let i = 0; i < arrayOne.length; i++){
      if (valueOne.includes(arrayOne[i])){
        opacity = opacity + 0.0715
      }
    }
    for (let i = 0; i < arrayTwo.length; i++){
      if (valueTwo.includes(arrayTwo[i])){
        opacity = opacity + 0.0715
      }
    }
    if (valueOne.includes(arrayOne[6]) && valueOne !== "Число 1") {
      opacity = opacity - (0.0715 * 7)
    }
    if (valueTwo.includes(arrayTwo[6]) && valueTwo !== "Число 2") {
      opacity = opacity - (0.0715 * 7)
    }
    this.setState({
      easterEggBeginButtonOpacity: opacity
    })
  }

  showQuestion(){
    this.setState({
      easterEggShowQuestion: true
    })
    ReactDOM.findDOMNode(this.input1Ref.current).value = "";
    ReactDOM.findDOMNode(this.input2Ref.current).value = "";
    this.setState({
      easterEggBeginButtonOpacity: 0
    })
  }

  hideQuestion(text){
    if (text){
      this.setState({
        easterEggShowQuestion: false
      })
      this.toggleErrorAlert(text)
    } else {
      this.setState({
        easterEggShowQuestion: false,
        easterEggShowMem: true
      })
      this.setCheckToDone("EGG")
    }
  }

  containsSpecialChars(str){
    const chars = "!\"№;%:?*()_+={}[]'/|\\".split("");
    for (let i = 0; i < chars.length; i++){
      if (str.includes(chars[i])){
        return true
      }
    }
    return false
  }

  containsDotChars(str){
    const chars = ".,".split("");
    for (let i = 0; i < chars.length; i++){
      if (str.includes(chars[i])){
        return true
      }
    }
    return false
  }

  containsWrongMinus(str){
    if (str.includes("-")){
      if (str.indexOf("-") !== 0 || str.split("-").length > 2){
        return true
      }
    }
    return false
  }

  countResult(f, s , type){
    let t;
    switch (type) {
      case "plus":
        t = f + s
        break;
      case "deduct":
        t = f - s
        break;
      case "multiply":
        t = f * s
        break;
      case "divide":
        t = f / s
        break;
    
      default:
        break;
    }
    this.setState({
      outputValue: "" + t
    })
  }

  verifyInput(type){
    let valueOne = ReactDOM.findDOMNode(this.input1Ref.current).value;
    let valueTwo = ReactDOM.findDOMNode(this.input2Ref.current).value;
    console.log(valueOne, valueTwo, type)
    if (valueOne === "" || valueTwo === ""){
      this.setCheckToDone("emptyFields")
      this.toggleErrorAlert("Пожалуйста, заполните все поля")
      return
    }
    if (this.containsSpecialChars(valueOne) || this.containsSpecialChars(valueTwo)
    || this.containsWrongMinus(valueOne) || this.containsWrongMinus(valueTwo)){
      this.setCheckToDone("specialChars")
      this.toggleErrorAlert("Символы !\"№;%:?*()_+=-{}[]'/|\\ запрещены")
      return
    }
    let intOne = parseInt(valueOne);
    let intTwo = parseInt(valueTwo);
    if (isNaN(intOne) || isNaN(intTwo) || valueOne.match(/[a-zA-Zа-яА-Я]+/) !== null || valueTwo.match(/[a-zA-Zа-яА-Я]+/) !== null){
      this.setCheckToDone("nonNumeric")
      this.toggleErrorAlert("Недопустимые символы")
      return
    }
    if (this.containsDotChars(valueOne) || this.containsDotChars(valueTwo)){
      this.setCheckToDone("floatDot")
      this.toggleErrorAlert("Допустимы только целые числа")
      return
    }
    if (intTwo === 0 && type === "divide"){
      this.setCheckToDone("divideByZero")
      this.toggleErrorAlert("На 0 делить нельзя")
      return
    }
    if (intOne > 100 || intTwo > 100){
      this.setCheckToDone("aboveUpper")
      this.toggleErrorAlert("Рассчеты производятся в промежутке от -100 до 100")
      return
    }
    if (intOne < -100 || intTwo < -100){
      this.setCheckToDone("belowLower")
      this.toggleErrorAlert("Рассчеты производятся в промежутке от -100 до 100")
      return
    }
    if (intOne === 0 && intTwo === 0){
      this.setCheckToDone("zeroAndZero")
      this.countResult(intOne, intTwo, type)
    }
    if ((intOne === 0 && (-100 <= intTwo && intTwo <= 100 && intTwo !== 0)) 
    || (intTwo === 0 && (-100 <= intOne && intOne <= 100 && intOne !== 0))){
      this.setCheckToDone("zeroAndNormal")
      this.countResult(intOne, intTwo, type)
    }
    if ((0 < intTwo && intTwo < 100) ||  (0 < intOne && intOne < 100)){
      this.setCheckToDone("middleValuesAboveZero")
      this.countResult(intOne, intTwo, type)
    }
    if ((-100 < intTwo && intTwo < 0) ||  (-100 < intOne && intOne < 0)){
      this.setCheckToDone("middleValuesBelowZero")
      this.countResult(intOne, intTwo, type)
    }
    if (intOne === 100 || intTwo === 100){
      this.setCheckToDone("upper")
      this.countResult(intOne, intTwo, type)
    }
    if (intOne === -100 || intTwo === -100){
      this.setCheckToDone("lower")
      this.countResult(intOne, intTwo, type)
    }
    if (-100 <= intTwo && intTwo < 0 && type === "deduct"){
      this.setCheckToDone("deductBelowZero")
      this.countResult(intOne, intTwo, type)
    }
    if (type === "plus"){
      this.setCheckToDone("plus", true)
    }
    if (type === "deduct"){
      this.setCheckToDone("deduct", true)
    }
    if (type === "multiply"){
      this.setCheckToDone("multiply", true)
    }
    if (type === "divide"){
      this.setCheckToDone("divide", true)
    }
  }

  render() {
    return (
      this.state.allDone ?
      <Container>
        <Row>
          <Col>
            <h1 class="display-4 text-center">Поздравляю!<br />Вы прошли все тесты и нашли пасхалочку<br /><FontAwesomeIcon icon={faSmileWink} style={{color:"#e0d528"}}/>
            <br /> С днем тестировщика!!!
            </h1>
          </Col>
        </Row>
      </Container>
      :
      <Container>
        <Row>
          <Col xl="8" className="mt-3">
          <h3>Простейший калькулятор</h3>
          <p className="text-justify">{"Дано два поля, и выбор дейтсвия: сложить, вычесть, умножить, поделить. " + 
            "При нажатии на кнопку с действием происходит выполнение выражения: <Число 1><Действие><Число 2>. " + 
            "Значения, принимаемые полями, — целые числа в диапазоне от -100 до 100. " +
            "В поле “Пусто” выводится результат выполнения действия. " +
            "Валидации в режиме реального времени не предусмотрено."}</p>
          </Col>
          <Col xl="4" className="mt-3">
            <h3>Задание:</h3>
            <p>Протестируйте форму</p>
          </Col>
        </Row>
        <Row>
          <Col xl="8" className="mt-3">
            <Alert color="success" isOpen={this.state.successAlert}>
              Правильный тест! (я так решил)
            </Alert>
          <Form>
            <FormGroup>
              <InputGroup>
                <Input 
                  name="val1" 
                  placeholder="Число 1" 
                  defaultValue="" 
                  ref={this.input1Ref} 
                  onChange={this.verifyEasterEgg.bind(this)}/>
                <Input 
                  name="val2" 
                  placeholder="Число 2" 
                  defaultValue="" 
                  ref={this.input2Ref} 
                  onChange={this.verifyEasterEgg.bind(this)}/>
                <InputGroupAddon addonType="append">{this.state.outputValue ? this.state.outputValue : "Пусто"}</InputGroupAddon>
              </InputGroup>
            </FormGroup>
            {this.state.easterEggShowQuestion? 
              <Card body inverse style={{ backgroundColor: '#333', borderColor: '#333' }} className="mb-2">
                <CardTitle className="text-center">Внимание вопрос</CardTitle>
                <CardText className="text-center">
                  А ты <span style={{opacity: 0.9}} onClick={this.hideQuestion.bind(this, null)}>точно</span> тестировщик?
                </CardText>
                <div className="text-center">
                  <ButtonGroup size="sm">
                    <Button onClick={this.hideQuestion.bind(this, "Ну нет, я тебе не верю")}>Да</Button>
                    <Button onClick={this.hideQuestion.bind(this, "Эх, я уж было поверил :(")}>Нет</Button>
                  </ButtonGroup>
                </div>
              </Card>:""
            }
            {this.state.easterEggShowMem ?
              <div className="text-center mb-2">
                <img 
                  onClick={() => this.setState({easterEggShowMem: false})}
                  src="http://risovach.ru/upload/2014/04/mem/krestnyy-otec_49281027_orig_.jpeg" 
                  alt="Мем про тестировщиков"/>
              </div>:""
            }
            <Alert color="danger"  isOpen={this.state.showAlert}>
              {this.state.alertText}
            </Alert>
            <FormGroup>
              <div className="text-center">
              <ButtonGroup size="sm">
                <Button onClick={this.verifyInput.bind(this, "plus")}>Сложить</Button>
                <Button onClick={this.verifyInput.bind(this, "deduct")}>Вычесть</Button>
                <Button onClick={this.verifyInput.bind(this, "multiply")}>Умножить</Button>
                <Button onClick={this.verifyInput.bind(this, "divide")}>Разделить</Button>
                {this.state.easterEggBeginButtonOpacity ?
                  <Button 
                    disabled={this.state.easterEggBeginButtonOpacity<1} 
                    color="success" 
                    onClick={this.showQuestion.bind(this)}
                    style={{opacity: this.state.easterEggBeginButtonOpacity, transition: "3s"}}>
                    Действие
                  </Button>:""
                }
              </ButtonGroup>
              </div>
            </FormGroup>
          </Form>
          </Col>
          <Col xl="4" className="mt-3">
            <h5>Результаты:</h5>
            {this.state.checks &&
              <ListGroup>
              {this.state.checks.map((item, index) => {
                return (
                  <ListGroupItem key={index} className="justify-content-between">
                    {item.type === "EGG" ?
                    (item.isDone ?
                    <FontAwesomeIcon icon={faStarOfDavid} style={styles.doneCheck}/>:<FontAwesomeIcon icon={faStarHalf} style={styles.undoneCheck}/>)
                    :
                    (item.isDone ?
                    <FontAwesomeIcon icon={faCheck} style={styles.doneCheck}/>:<FontAwesomeIcon icon={faQuestion} style={styles.undoneCheck}/>)
                    }
                    <span style={{paddingLeft: 5}}>
                    {item.isDone?
                      item.text:"???"
                    }
                    </span>
                  </ListGroupItem>
                )
              })}
              </ListGroup>
            }
          </Col>
        </Row>
      </Container>
    );
  }
}

const styles = {
  doneCheck: {
    color: "green"
  },
  undoneCheck:{
    color: "gray",
    opacity: "70%"
  }
}

export default App;
